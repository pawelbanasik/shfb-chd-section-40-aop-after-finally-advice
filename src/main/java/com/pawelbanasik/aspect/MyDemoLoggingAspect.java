package com.pawelbanasik.aspect;

import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.pawelbanasik.domain.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {

	@After("execution(* com.pawelbanasik.dao.AccountDao.findAccounts(..))")
	public void afterFinallyFindAccountAdvice(JoinPoint joinPoint) {
		String methodSignature = joinPoint.getSignature().toShortString();
		System.out.println("\n=====> Executing @After finally: " + methodSignature);
	}
	
	@AfterThrowing(pointcut = "execution(* com.pawelbanasik.dao.AccountDao.findAccounts(..))", throwing = "theExc")
	public void afterThrowingFindAccountsAdvice(JoinPoint joinPoint, Throwable theExc) {
		String methodSignature = joinPoint.getSignature().toShortString();
		System.out.println("\n=====> Executing @AfterThrowing on metod: " + methodSignature);
		System.out.println("\n===> The exception is: " + theExc);
	}

	@AfterReturning(pointcut = "execution(* com.pawelbanasik.dao.AccountDao.findAccounts(..))", returning = "result")
	public void afterReturningFindAdvice(JoinPoint joinPoint, List<Account> result) {
		String methodSignature = joinPoint.getSignature().toShortString();
		System.out.println("\n=====> Executing @AfterReturning on metod: " + methodSignature);
		System.out.println("\n=====> result is: " + result);
		convertAccountNamesToUpperCase(result);
		System.out.println("\n=====> result is: " + result);
	}

	private void convertAccountNamesToUpperCase(List<Account> result) {
		for (Account account : result) {
			String toUpperName = account.getName().toUpperCase();
			account.setName(toUpperName);
		}

	}

	@Before("com.pawelbanasik.aspect.LuvAopExpressions.forDaoPackageNoGetterSetter()")
	public void beforeAddAccountAdvice(JoinPoint joinPoint) {
		System.out.println("\n=====> Executing @Before advice on addAccount()");
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		System.out.println("Method: " + methodSignature);
		Object[] args = joinPoint.getArgs();
		for (Object tempArg : args) {
			System.out.println(tempArg);
			if (tempArg instanceof Account) {
				Account account = (Account) tempArg;
				System.out.println("account name: " + account.getName());
				System.out.println("account level: " + account.getLevel());
			}

		}

	}
}
