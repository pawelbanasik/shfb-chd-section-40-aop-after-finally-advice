package com.pawelbanasik.main;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.pawelbanasik.config.DemoConfig;
import com.pawelbanasik.dao.AccountDao;
import com.pawelbanasik.domain.Account;

public class AfterReturningDemoApp {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		AccountDao accountDao = context.getBean("accountDao", AccountDao.class);
		List<Account> accounts = accountDao.findAccounts(false);
		System.out.println("\n\nMain Program: AfterReturningDemoApp");
		System.out.println("----");
		System.out.println(accounts);
		context.close();

	}

}
