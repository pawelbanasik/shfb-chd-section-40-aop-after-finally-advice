package com.pawelbanasik.dao;

import org.springframework.stereotype.Repository;

@Repository
public class MembershipDao {

	public boolean addSillyMember() {
		System.out.println(getClass().getSimpleName() + ": Doing stuff: adding a membership account.");
		return true;
	}
	
	public void goToSleep() {
		System.out.println(getClass().getSimpleName() + ": I'm going to sleep.");
	}
	
}
